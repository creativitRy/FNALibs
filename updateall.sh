#!/usr/bin/env bash

set -euo pipefail
set -x

if [ "$#" -ne 1 ]; then
    echo "Usage: $0 TAG.NUMBER.HERE"
    exit 1
fi

echo Updating FNA...

cd FNA
git checkout "$1"
cd ..

echo Updating fnalibs...

rm -rf fnalibs
mkdir fnalibs
curl --show-error --location https://fna.flibitijibibo.com/archive/fnalibs.tar.bz2 | tar -xjf - -C fnalibs

echo Done
