# FNALibs

Mirror that combines https://github.com/FNA-XNA/FNA and build outputs of https://github.com/FNA-XNA/FNA/tree/master/lib from https://fna.flibitijibibo.com/archive/fnalibs.tar.bz2

## License

FNA is released under the Microsoft Public License. See FNA/licenses/LICENSE for details.

FNA uses LzxDecoder.cs, released under a dual MSPL/LGPL license.
See FNA/licenses/lzxdecoder.LICENSE for details.

FNA uses code from the Mono.Xna project, released under the MIT license.
See FNA/licenses/monoxna.LICENSE for details.
